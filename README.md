# Auto_Web

## Listado de Actividades
- Pruebas exploratorias en la web
- Listado de Test Cases
- Seleccionar los Test Cases que seran automatizados
- Elegir las herramientas que se utilizaran 
- Iniciar con el desarrollo

## Herramientas Utilizadas
- Selenium
- Serenity BDD
- IntelliJ
- Java

## Test Cases:
La web no es lo suficientemente estable para implementar todos los escenarios planteados, es complicado elegir una
fecha y estar 100% de que hay asientos disponibles.
- Happy Path - Buscar tickets con destino a Cusco, ruta Puno -> Cusco
- Happy Path - Buscar tickets con destino a Cusco, ruta Arequipa -> Puno -> Cusco
- Unhappy Path - Buscar tickets con destino a Cusco, ruta Puno -> Cusco - No hay cabinas disponibles
- Happy Path - Buscar tickets con destino a Machu Picchu, ruta Cusco -> Machu Picchu - Solo ida
- Happy Path - Buscar tickets con destino a Machu Picchu, ruta Cusco -> Machu Picchu - Ida y vuelta
- Unhappy Path - No se completó toda la data antes de realizar la busqueda
- Unhappy Path - Se colocan mas de 9 pasajeron en la bsuqueda
- Unhappy Path - No hay boletos disponibles
- Happy Path - Completar el formulario de la tarjeta de credito
- Happy Path - Completar compra de pasajes de tren (Dataa prueba)