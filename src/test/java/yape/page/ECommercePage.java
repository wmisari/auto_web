package yape.page;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.pages.PageObject;
import org.junit.Assert;

public class ECommercePage extends PageObject {

    @FindBy(id = "form_ventas")
    private WebElementFacade ventasForm;

    @FindBy(xpath = "//*[@id='frm_viajes_bae']/div[3]/div[2]/div[2]/div[3]/div")
    private WebElementFacade suitCabins;

    @FindBy(xpath = "//*[@id='frm_viajes_bae']/div[3]/div[2]/div[3]/div[3]/div")
    private WebElementFacade twinBedCabins;

    @FindBy(xpath = "//*[@id='frm_viajes_bae']/div[3]/div[2]/div[4]/div[3]/div")
    private WebElementFacade bunkBedCabins;

    @FindBy(xpath = "//div[@class='contenido-trenes trenes']")
    private WebElementFacade trainContentForm;

    @FindBy(xpath = "//input[@class='btn  btn-continuar']")
    private WebElementFacade continueButton;

    @FindBy(id = "sb-player")
    private WebElementFacade popUp;

    public void validateVentasFormIsVisible(){
        Assert.assertTrue(this.ventasForm.isVisible());
    }

    public void validateTrainsFormIsVisible(){
        Assert.assertTrue(this.trainContentForm.isVisible());
    }

    public void validateCabins() {
        Assert.assertEquals("No cabins available",this.suitCabins.getText(), "No cabins available for the selected date");
    }

    public void clickContinueButton() throws InterruptedException {
        Thread.sleep(4000);
        this.continueButton.waitUntilVisible();
        this.continueButton.click();
    }

    public void validatePopUp(){
        this.popUp.isVisible();
        String message = this.popUp.getText();
        Assert.assertEquals(message, "Select your journey.");
    }
}
