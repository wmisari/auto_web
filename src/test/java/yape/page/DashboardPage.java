package yape.page;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.pages.PageObject;
import org.junit.Assert;

public class DashboardPage  extends PageObject {

    @FindBy(id = "destinoSelect")
    private WebElementFacade selectDestination;

    @FindBy(id = "rutaSelect")
    private WebElementFacade selectRoute;

    @FindBy(id = "cbTrenSelect")
    private WebElementFacade selectTrain;

    @FindBy(id = "countParentsChildren")
    private WebElementFacade selectPassengers;

    @FindBy(id = "adultsAume")
    private WebElementFacade moreAdults;

    @FindBy(id = "childrenAume")
    private WebElementFacade moreChildren;

    @FindBy(id = "btn_search")
    private WebElementFacade findButton;

    @FindBy(xpath = "//label[.='Round trip']")
    private WebElementFacade roundTripOption;

    @FindBy(xpath = "//label[.='One Way']")
    private WebElementFacade oneWayOption;

    @FindBy(id = "error-pasajeros")
    private WebElementFacade errorMessage;

    @FindBy(id = "error-inputs")
    private WebElementFacade errorInput;

    @FindBy(id = "adultsSelect")
    private WebElementFacade inputAdultPassangers;

    public void openTheBrowser() {
        this.openUrl("http://www.perurail.com/");
    }

    public void selectDestination(String destination){
        this.selectDestination.selectByVisibleText(destination);
    }

    public void selectRoute(String route){
        this.selectRoute.selectByVisibleText(route);
    }

    public void selectTrain(String train){
        this.selectTrain.selectByVisibleText(train);
    }

    public void findTicketTrains(){
        this.findButton.click();
    }

    public void selectRoundTripType(){
        this.roundTripOption.click();
    }

    public void selectOneWayType(){
        this.oneWayOption.click();
    }

    public void validateErrorInputMessage() {
        Assert.assertTrue(this.errorInput.isVisible());
    }

    public void validateErrorMessage() {
        Assert.assertTrue(this.errorMessage.isVisible());
    }

    public void exceedPassengers(){
        this.selectPassengers.click();
        this.inputAdultPassangers.clear();
        this.inputAdultPassangers.sendKeys("9");
        this.moreAdults.click();
        this.moreChildren.click();
    }
}
