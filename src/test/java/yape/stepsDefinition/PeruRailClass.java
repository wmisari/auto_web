package yape.stepsDefinition;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;
import yape.page.*;

public class PeruRailClass {

    @Steps
    DashboardPage dashboarPage;
    @Steps
    ECommercePage eCommercePage;

    @Given("^I navigated to the Peru Rail's web$")
    public void i_navigate_to_the_Peru_Rail_s_web() {
        dashboarPage.openTheBrowser();
    }


    @When("^I select the destination \"([^\"]*)\"$")
    public void i_select_the_destination(String destination) {
        dashboarPage.selectDestination(destination);
    }

    @And("^I select the \"([^\"]*)\" of the trip$")
    public void i_select_the_of_the_trip(String route) {
        dashboarPage.selectRoute(route);
    }

    @And("^I select the train \"([^\"]*)\"$")
    public void i_select_the_train(String train) {
        dashboarPage.selectTrain(train);
    }

    @Then("^I should see my options$")
    public void i_should_see_my_options() {
        dashboarPage.findTicketTrains();
        eCommercePage.validateVentasFormIsVisible();
    }

    @Then("^I should see the trains list$")
    public void i_should_see_the_trains_list() {
        dashboarPage.findTicketTrains();
        eCommercePage.validateTrainsFormIsVisible();
    }

    @Then("^I should see my options with no cabins available$")
    public void i_should_see_my_options_with_no_cabins_available() {
        dashboarPage.findTicketTrains();
        eCommercePage.validateVentasFormIsVisible();
        eCommercePage.validateCabins();
    }

    @And("^I select a type \"([^\"]*)\" for the trip$")
    public void i_select_a_type_for_the_trip(String type) {
        if (type.equals("Round Trip")){
            dashboarPage.selectRoundTripType();
        }
        else{
            dashboarPage.selectOneWayType();
        }
    }

    @Then("^I should not see the trains list$")
    public void i_should_not_see_the_trains_list() {
        dashboarPage.findTicketTrains();
        dashboarPage.validateErrorInputMessage();
    }

    @Then("^I select more than 9 passengers and I will see a warning message$")
    public void i_select_more_than_9_passengers_and_i_will_see_a_warning_message() {
        dashboarPage.exceedPassengers();
        dashboarPage.validateErrorMessage();
    }

    @And("^I follow the flow$")
    public void i_follow_the_flow() throws InterruptedException {
        dashboarPage.findTicketTrains();
        eCommercePage.clickContinueButton();
    }

    @Then("^A pop up message should appear$")
    public void a_pop_up_message_should_appear() {
        eCommercePage.validatePopUp();
    }
}

