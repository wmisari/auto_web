Feature: Booking Peru Rail

  Scenario Outline: Happy Path - Search tickets for the train Puno_Cusco
    Given I navigated to the Peru Rail's web
    When I select the destination "<destination>"
    And I select the "<route>" of the trip
    And I select the train "<train>"
    Then I should see my options
    Examples:
      |   destination  |      route     |               train             |
      |     Cusco      |  Puno > Cusco  | Andean Explorer, A Belmond Train|

  Scenario Outline: Happy Path - Search tickets for the train Arequipa_Cusco_Puno
    Given I navigated to the Peru Rail's web
    When I select the destination "<destination>"
    And I select the "<route>" of the trip
    Then I should see my options
    Examples:
      |   destination  |            route          |
      |     Cusco      |  Arequipa > Puno > Cusco  |

  Scenario Outline: Unhappy Path - Search tickets for the train Puno_Cusco - No cabins available
    Given I navigated to the Peru Rail's web
    When I select the destination "<destination>"
    And I select the "<route>" of the trip
    And I select the train "<train>"
    Then I should see my options with no cabins available
    Examples:
      |   destination  |      route     |               train             |
      |     Cusco      |  Puno > Cusco  | Andean Explorer, A Belmond Train|

  Scenario Outline: Happy Path - Search tickets for the train Cusco_Machu Picchu
    Given I navigated to the Peru Rail's web
    When I select the destination "<destination>"
    And I select the "<route>" of the trip
    And I select a type "<type>" for the trip
    Then I should see the trains list
    Examples:
      |   destination  |          route         |      type      |
      |  Machu Picchu  |  Cusco > Machu Picchu  |   Round Trip   |
      |  Machu Picchu  |  Cusco > Machu Picchu  |   One Way      |

  Scenario Outline: Unhappy Path - Search tickets - Data not complete
    Given I navigated to the Peru Rail's web
    When I select the destination "<destination>"
    And I select the "<route>" of the trip
    Then I should not see the trains list
    Examples:
      |   destination  |     route      |
      |     Cusco      |  Puno > Cusco  |

  Scenario Outline: Unhappy Path - Search tickets - Excess passengers
    Given I navigated to the Peru Rail's web
    When I select the destination "<destination>"
    And I select the "<route>" of the trip
    Then I select more than 9 passengers and I will see a warning message
    Examples:
      |   destination  |          route         |
      |  Machu Picchu  |  Cusco > Machu Picchu  |

  Scenario Outline: Unhappy Path - Search tickets - Sold Out
    Given I navigated to the Peru Rail's web
    When I select the destination "<destination>"
    And I select the "<route>" of the trip
    And I follow the flow
    Then A pop up message should appear
    Examples:
      |   destination  |          route         |
      |  Machu Picchu  |  Cusco > Machu Picchu  |